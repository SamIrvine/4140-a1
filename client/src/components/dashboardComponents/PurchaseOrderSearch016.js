import React, { useState } from "react";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import DatabaseApi from "../../api/DatabaseApi";
import Typography from "@material-ui/core/Typography";

const PurchaseOrderSearch016 = () => {
  const [poNo016, setPoNo016] = useState("");
  const [poLines, setPoLines] = useState([]);

  const handleChange = (event) => {
    setPoNo016(event.target.value);
  };

  const handleSearch = () => {
    // make database api for req for lines here
    DatabaseApi.getAllLines016(poNo016)
      .then((lines) => {
        setPoLines(lines.data);
      })
      .catch((err) => {
        console.error("error fetching lines for poNo016: " + poNo016);
      });
  };

  return (
    <div style={{ width: "100%" }}>
      <div
        style={{
          display: "flex",
          justifyContent: "center",
          width: "100%",
          margin: "16px",
        }}
      >
        <TextField
          label="Purchase Order Number"
          variant="outlined"
          onChange={handleChange}
          type="number"
          InputProps={{ inputProps: { min: 0 } }}
        ></TextField>
        <Button variant="outlined" onClick={handleSearch}>
          Search
        </Button>
      </div>
      <TableContainer component={Paper}>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>Line Number</TableCell>
              <TableCell align="right">Price of Line</TableCell>
              <TableCell align="right">Order Created</TableCell>
              <TableCell align="right">Part Number</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {!!poLines &&
              (poLines || []).map((line) => {
                return (
                  <TableRow key={line.lineNo016}>
                    <TableCell component="th" scope="row">
                      {line.lineNo016}
                    </TableCell>
                    <TableCell align="right">${line.linePrice016}</TableCell>
                    <TableCell align="right">{line.createdAt}</TableCell>
                    <TableCell align="right">{`${line.partNo016}`}</TableCell>
                  </TableRow>
                );
              })}
            {poLines.length === 0 && (
              <Typography style={{ padding: " 16px" }}>
                No Lines exist for this purchase order
              </Typography>
            )}
          </TableBody>
        </Table>
      </TableContainer>
    </div>
  );
};

export default PurchaseOrderSearch016;
