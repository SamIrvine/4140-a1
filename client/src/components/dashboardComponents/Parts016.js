import React, { useEffect, useState } from "react";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import Button from "@material-ui/core/Button";
import DatabaseApi from "../../api/DatabaseApi.js";
import UpdatePartDialog016 from "./updateDialogs/UpdatePartDialog016";

const Parts = ({ isSpecificUpdateOpen016 }) => {
  const [allParts, setAllParts] = useState([]);
  const [updatePartNo016, setUpdatePartNo016] = useState();
  const [updateCurrentPrice016, setUpdateCurrentPrice016] = useState();
  const [updateQty016, setUpdateQty016] = useState();
  const [isUpdateOpen016, setIsUpdateOpen016] = useState(false);

  const fetchAllParts016 = () => {
    DatabaseApi.getAllParts016()
      .then((allParts) => {
        setAllParts(allParts.data);
      })
      .catch((err) => {
        console.error("error fetching all parts: " + err);
      });
  };
  useEffect(() => {
    if (!isUpdateOpen016) {
      setTimeout(fetchAllParts016, 500);
    }
    fetchAllParts016();
  }, [setAllParts, isUpdateOpen016, isSpecificUpdateOpen016]);

  const onUpdatePart016 = (partNo016, currentPrice016, qty016) => {
    setUpdatePartNo016(partNo016);
    setUpdateCurrentPrice016(currentPrice016);
    setUpdateQty016(qty016);
    setIsUpdateOpen016(true);
  };

  const handleClose016 = () => {
    setUpdatePartNo016("");
    setUpdateCurrentPrice016("");
    setUpdateQty016("");
    setIsUpdateOpen016(false);
  };

  return (
    <div>
      <TableContainer component={Paper}>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>Part Number</TableCell>
              <TableCell align="right">Part Name</TableCell>
              <TableCell align="right">Current Price</TableCell>
              <TableCell align="right">Part Description</TableCell>
              <TableCell align="right">Quantity</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {!!allParts &&
              (allParts || []).map((parts) => (
                <TableRow key={parts.partNo016}>
                  <TableCell component="th" scope="row">
                    {parts.partNo016}
                  </TableCell>
                  <TableCell align="right">{parts.partName016}</TableCell>
                  <TableCell align="right">{`$${parts.currentPrice016}`}</TableCell>
                  <TableCell align="right">
                    {parts.partDescription016}
                  </TableCell>
                  <TableCell align="right">{parts.qty016}</TableCell>
                  <TableCell>
                    <Button
                      variant="contained"
                      color="secondary"
                      onClick={() => {
                        onUpdatePart016(
                          parts.partNo016,
                          parts.currentPrice016,
                          parts.qty016
                        );
                      }}
                    >
                      Update
                    </Button>
                  </TableCell>
                </TableRow>
              ))}
          </TableBody>
        </Table>
      </TableContainer>
      <UpdatePartDialog016
        isOpen016={isUpdateOpen016}
        onClose016={handleClose016}
        partNo016={updatePartNo016}
        currentPrice016={updateCurrentPrice016}
        qty016={updateQty016}
      />
    </div>
  );
};

export default Parts;
