import React, { useEffect, useState } from "react";
import DialogTitle from "@material-ui/core/DialogTitle";
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import DialogActions from "@material-ui/core/DialogActions";
import InputAdornment from "@material-ui/core/InputAdornment";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import DatabaseApi from "../../../api/DatabaseApi";

const UpdatePartDialog016 = ({
  isOpen016,
  onClose016,
  partNo016,
  currentPrice016,
  qty016,
}) => {
  const [newPartNo016, setNewPartNo016] = useState(partNo016);
  const handleChangeNewPartNo016 = (event) => {
    setNewPartNo016(event.target.value);
  };

  const [newCurrentPrice016, setNewCurrentPrice016] = useState(currentPrice016);
  const handleChangeNewCurrentPrice016 = (event) => {
    setNewCurrentPrice016(event.target.value);
  };

  const [newQty016, setNewQty016] = useState(qty016);
  const handleChangeNewQty016 = (event) => {
    setNewQty016(event.target.value);
  };

  const [showInvalidPartNo016, setShowInvalidPartNo016] = useState(false);

  useEffect(() => {
    if (isOpen016) {
      setNewPartNo016(partNo016);
      setNewCurrentPrice016(currentPrice016);
      setNewQty016(qty016);
      setOldCurrentPrice016(currentPrice016);
      setOldQty016(qty016);
    }
  }, [isOpen016, partNo016, currentPrice016, qty016]);

  const handleClose = () => {
    // Do any other necessary logic on close here
    setOnReview016(false);
    setShowInvalidPartNo016(false);
    setNewPartNo016("");
    setNewCurrentPrice016("");
    setNewQty016("");
    onClose016();
  };

  const [oldCurrentPrice016, setOldCurrentPrice016] = useState(currentPrice016);
  const [oldQty016, setOldQty016] = useState(qty016);

  const [onReview016, setOnReview016] = useState(false);
  const handleOnReview016 = () => {
    getOriginalValues016();

    setOnReview016(!onReview016);
  };

  const getOriginalValues016 = () => {
    DatabaseApi.getOnePart016(newPartNo016).then((part) => {
      if (!part.data) {
        setShowInvalidPartNo016(true);
      }
      setOldCurrentPrice016(part.data.currentPrice016);
      setOldQty016(part.data.qty016);
    });
  };

  const onGoBack016 = () => {
    setOnReview016(false);
    setShowInvalidPartNo016(false);
  };

  const onSubmit016 = () => {
    // update the part with the new values
    DatabaseApi.updatePart016(newPartNo016, newCurrentPrice016, newQty016);
    handleClose();
  };

  const isMissingFields = !newPartNo016 || !newCurrentPrice016 || !newQty016;

  return (
    <Dialog open={isOpen016} onClose={handleClose}>
      <DialogTitle>
        {onReview016
          ? `Confirm Changes for partNo: ${newPartNo016}`
          : `Update Part ${!!partNo016 ? partNo016 : ""}`}
      </DialogTitle>
      <DialogContent style={{ display: "flex" }}>
        {onReview016 ? (
          showInvalidPartNo016 ? (
            <div>
              <Typography color="error" style={{ marginBottom: "16px" }}>
                <strong>
                  Part number entered does not match any records, please refer
                  to the all parts table to find the part number.
                </strong>
              </Typography>
            </div>
          ) : (
            <div>
              <Typography style={{ marginBottom: "16px" }}>
                {`Old Price: $${oldCurrentPrice016} --> New Price: `}
                <strong>${newCurrentPrice016}</strong>
              </Typography>
              <Typography style={{ marginBottom: "16px" }}>
                {`Old Qty: ${oldQty016} --> New Qty: `}
                <strong>{newQty016}</strong>
              </Typography>
            </div>
          )
        ) : (
          <>
            <div style={{ padding: "16px" }}>
              <TextField
                label="Enter Part Number"
                InputProps={{ inputProps: { min: 0 } }}
                variant="outlined"
                type="number"
                required
                disabled={!!partNo016}
                value={newPartNo016}
                onChange={handleChangeNewPartNo016}
              />
            </div>
            <div style={{ padding: "16px" }}>
              <TextField
                label="Enter New Price"
                variant="outlined"
                type="number"
                InputProps={{
                  startAdornment: (
                    <InputAdornment position="start">$</InputAdornment>
                  ),
                  inputProps: { min: 0 },
                }}
                required
                value={newCurrentPrice016}
                onChange={handleChangeNewCurrentPrice016}
              />
            </div>
            <div style={{ padding: "16px" }}>
              <TextField
                label="Enter New Qty"
                variant="outlined"
                type="number"
                InputProps={{ inputProps: { min: 0 } }}
                required
                value={newQty016}
                onChange={handleChangeNewQty016}
              />
            </div>
          </>
        )}
      </DialogContent>
      <DialogActions>
        {onReview016 ? (
          <div>
            {!showInvalidPartNo016 && (
              <Button
                style={{ marginRight: "8px" }}
                variant="outlined"
                color="primary"
                onClick={onSubmit016}
              >
                Submit
              </Button>
            )}
            <Button variant="outlined" onClick={onGoBack016}>
              Go Back
            </Button>
          </div>
        ) : (
          <Button
            color="secondary"
            variant="outlined"
            onClick={handleOnReview016}
            disabled={isMissingFields}
          >
            {isMissingFields ? "Fill all fields" : "Review Changes"}
          </Button>
        )}
      </DialogActions>
    </Dialog>
  );
};

export default UpdatePartDialog016;
