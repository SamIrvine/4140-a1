import React, { useEffect, useState } from "react";
import DialogTitle from "@material-ui/core/DialogTitle";
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import DialogActions from "@material-ui/core/DialogActions";
import Snackbar from "@material-ui/core/Snackbar";
import Alert from "@material-ui/lab/Alert";
import Typography from "@material-ui/core/Typography";
import DatabaseApi from "../../../api/DatabaseApi";
import Tooltip from "@material-ui/core/Tooltip";
import HelpIcon from "@material-ui/icons/HelpOutline";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import _ from "lodash";
import CancelIcon from "@material-ui/icons/Cancel";
import FilledIcon from "@material-ui/icons/CheckCircleOutline";
import Button from "@material-ui/core/Button";

const InsertPurchaseDialog016 = ({ isOpen016, onClose016, order016 }) => {
  const [successOpen, setSuccessOpen] = useState(false);
  const [errorOpen, setErrorOpen] = useState(false);
  const [warning, setWarning] = useState(false);
  const [clientInfo, setClientInfo] = useState({});
  const [poLines, setPoLines] = useState([]);
  // On mount fetch client info and associated lines for PO
  useEffect(() => {
    // Get the client info
    DatabaseApi.getOneClient016(order016.clientCompId016)
      .then((client) => {
        setClientInfo(client.data);
      })
      .catch((err) => {
        console.error("error fetching client: " + err);
      });

    // Get the PO lines
    DatabaseApi.getAllLines016(order016.poNo016)
      .then((lines) => {
        let linesObject = {};

        console.log(lines);

        lines.data.forEach((line) => {
          linesObject[line.partNo016] = {
            ...line,
            quantity016: !!linesObject[line.partNo016]
              ? linesObject[line.partNo016].quantity016 + 1
              : 1,
          };
        });
        console.log(Object.values(linesObject));
        setPoLines(Object.values(linesObject));
      })
      .catch((err) => {
        console.error(
          "error fetching lines for poNo016: " + order016.poNo016 + err
        );
      });
  }, [order016]);

  useEffect(async () => {
    if (poLines.length > 0) {
      const updatedPoLines = [];
      for (const line of poLines) {
        await DatabaseApi.getOnePart016(line.partNo016).then((part) => {
          updatedPoLines.push({ ...part.data, ...line });

          if (line.quantity016 > part.data.qty016) {
            setWarning(true);
          }
        });
      }

      if (!_.isEqual(poLines, updatedPoLines)) setPoLines(updatedPoLines);
    }
  }, [poLines]);

  const handleClose = () => {
    // Do any other necessary logic on close here
    onClose016();
    setWarning(false);
    setPoLines([]);
    setClientInfo({});
  };

  const handleProcessOrder = () => {
    // Do any other necessary logic on close here
    DatabaseApi.handleProcessPurchaseOrder016(order016.poNo016, poLines).then(
      (status) => {
        if (status.data.message.includes("filled")) {
          setSuccessOpen(true);
        } else {
          setErrorOpen(true);
        }
      }
    );
    handleClose();
  };

  const handleToastClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }

    setSuccessOpen(false);
    setErrorOpen(false);
  };

  return (
    <>
      <Dialog maxWidth="lg" open={isOpen016} onClose={handleClose}>
        <DialogTitle>Process Purchase Order</DialogTitle>

        <DialogContent>
          <div
            style={{
              display: "flex",
              flexDirection: "column",
              alignItems: "flex-start",
            }}
          >
            <Typography color="textPrimary" variant="h6">
              Client Info for{" "}
              {`${!!clientInfo ? clientInfo.clientCompName016 : ""} | City: ${
                !!clientInfo.clientCity016
                  ? clientInfo.clientCity016
                  : "loading..."
              } `}
            </Typography>
            <Typography color="textPrimary" variant="h6">
              {`Purchase Order Number: ${order016.poNo016} | `}{" "}
              <strong style={{ color: "green" }}>
                {" "}
                {`Money Owed: $${clientInfo.moneyOwed016}`}
              </strong>
            </Typography>
            <Tooltip
              title="PO lines that contain the same part have been merged for viewing purposes"
              arrow
              placement="top"
              style={{ fontSize: "16px" }}
            >
              <div
                style={{
                  marginTop: "16px",
                  display: "flex",
                  alignItems: "flex-start",
                  cursor: "pointer",
                }}
              >
                <Typography
                  color="textSecondary"
                  style={{ marginRight: "8px" }}
                >
                  Note on Line Items{" "}
                </Typography>
                <HelpIcon />
              </div>
            </Tooltip>
          </div>

          <TableContainer component={Paper}>
            <Table>
              <TableHead>
                <TableRow>
                  <TableCell>Current Part Price</TableCell>
                  <TableCell align="right">PO Part Price</TableCell>
                  <TableCell align="right">PO Quantity</TableCell>
                  <TableCell align="right">Part Quantity Available</TableCell>
                  <TableCell align="right">Part Number</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {!!poLines &&
                  (poLines || []).map((line) => {
                    return (
                      <TableRow key={line.lineNo016}>
                        <TableCell component="th" scope="row">
                          ${line.currentPrice016}
                        </TableCell>
                        <TableCell align="right">
                          ${line.linePrice016}
                        </TableCell>
                        <TableCell align="right">{line.quantity016}</TableCell>
                        <TableCell
                          style={{
                            color:
                              line.quantity016 > line.qty016 ? "red" : "green",
                          }}
                          align="right"
                        >
                          {line.qty016}
                        </TableCell>
                        <TableCell align="right">{`${line.partNo016}`}</TableCell>
                        <TableCell align="right">
                          {line.quantity016 > line.qty016 ? (
                            <CancelIcon style={{ color: "red" }} />
                          ) : (
                            <FilledIcon
                              style={{
                                width: "28px",
                                height: "auto",
                                color: "green",
                              }}
                            />
                          )}
                        </TableCell>
                      </TableRow>
                    );
                  })}
                {poLines.length === 0 && (
                  <Typography style={{ padding: " 16px" }}>
                    No Lines exist for this purchase order
                  </Typography>
                )}
              </TableBody>
            </Table>
          </TableContainer>
          <DialogActions style={{ marginTop: "16px" }}>
            <Button
              onClick={handleProcessOrder}
              style={{
                backgroundColor: warning ? "red" : "green",
                color: "white",
              }}
              variant="contained"
            >
              Process Order
            </Button>
          </DialogActions>
        </DialogContent>
      </Dialog>
      <Snackbar
        open={successOpen}
        autoHideDuration={6000}
        onClose={handleToastClose}
      >
        <Alert severity="success">
          Purchase order has been successfully processed
        </Alert>
      </Snackbar>
      <Snackbar
        open={errorOpen}
        autoHideDuration={6000}
        onClose={handleToastClose}
      >
        <Alert severity="error">
          Purchase Order was rolled back due to insufficient quantity
        </Alert>
      </Snackbar>
    </>
  );
};

export default InsertPurchaseDialog016;
