import React, { useState } from "react";
import DialogTitle from "@material-ui/core/DialogTitle";
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import DialogActions from "@material-ui/core/DialogActions";
import Button from "@material-ui/core/Button";
import DeleteIcon from "@material-ui/icons/Delete";
import Snackbar from "@material-ui/core/Snackbar";
import Alert from "@material-ui/lab/Alert";

import TextField from "@material-ui/core/TextField";
import { Typography } from "@material-ui/core";
import DatabaseApi from "../../../api/DatabaseApi";

const InsertPurchaseDialog016 = ({ isOpen016, onClose016 }) => {
  const [partNo016, setPartNo016] = useState();
  const [partNos, setPartNos] = useState([]);
  const [open, setOpen] = useState(false);
  const [errorOpen, setErrorOpen] = useState(false);

  const handleClose = () => {
    // Do any other necessary logic on close here
    setPartNo016();
    setPartNos([]);
    onClose016();
  };

  const handleChange = (event) => {
    setPartNo016(event.target.value);
  };

  const handleAddPart = () => {
    let newPartNos = [...partNos];
    newPartNos.push(partNo016);
    setPartNos(newPartNos);
  };
  const handleRemovePart = (index) => {
    let newPartNos = [...partNos];
    newPartNos.splice(index, 1);
    setPartNos(newPartNos);
  };

  const handleSubmitPurchaseOrder = () => {
    DatabaseApi.handleCreatePurchaseOrder016(
      window.sessionStorage.getItem("clientCompId016")
    ).then((purchaseOrder) => {
      // for each part no get the part
      partNos.forEach((partNo016) => {
        DatabaseApi.getOnePart016(partNo016).then((part) => {
          // create the line with the part price
          DatabaseApi.handleCreateLine016(
            purchaseOrder.data.poNo016,
            partNo016,
            part.data.currentPrice016
          )
            .then(() => {
              setOpen(true);
            })
            .catch(() => {
              setErrorOpen(true);
            });
        });
      });
    });
    handleClose();
  };

  const handleToastClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }

    setOpen(false);
    setErrorOpen(false);
  };

  return (
    <>
      <Dialog open={isOpen016} onClose={handleClose}>
        <DialogTitle>Insert Purchase Order</DialogTitle>

        <div
          style={{ display: "flex", padding: "16px 24px", paddingTop: "0px" }}
        >
          <Button
            onClick={handleSubmitPurchaseOrder}
            color="primary"
            variant="contained"
          >
            Place Order
          </Button>
        </div>
        <DialogContent>
          <div style={{ display: "flex" }}>
            <TextField
              label="Part Number"
              variant="outlined"
              onChange={handleChange}
              type="number"
              InputProps={{ inputProps: { min: 0 } }}
            ></TextField>
            <Button
              variant="contained"
              color="secondary"
              onClick={handleAddPart}
            >
              Add
            </Button>
          </div>
          <div style={{ marginTop: "16px" }}>
            {partNos.map((partNo016, index) => (
              <div
                key={index}
                style={{ display: "flex", alignItems: "center" }}
              >
                <Typography
                  variant="h5"
                  color="textSecondary"
                  style={{ marginRight: "8px" }}
                >{`1 x Part Number ${partNo016} Added`}</Typography>
                <DeleteIcon
                  onClick={() => {
                    handleRemovePart(index);
                  }}
                  style={{ cursor: "pointer" }}
                ></DeleteIcon>
              </div>
            ))}
          </div>
        </DialogContent>
        <DialogActions></DialogActions>
      </Dialog>
      <Snackbar open={open} autoHideDuration={6000} onClose={handleToastClose}>
        <Alert severity="success">Order creation success!</Alert>
      </Snackbar>
      <Snackbar
        open={errorOpen}
        autoHideDuration={6000}
        onClose={handleToastClose}
      >
        <Alert severity="error">
          Some line items weren't added because part numbers don't exist
        </Alert>
      </Snackbar>
    </>
  );
};

export default InsertPurchaseDialog016;
