import React, { useEffect, useState } from "react";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import DatabaseApi from "../../api/DatabaseApi.js";

const Clients = () => {
  const [allClients, setAllClients] = useState({});
  useEffect(() => {
    DatabaseApi.getAllClients016()
      .then((allClients) => {
        setAllClients(allClients);
      })
      .catch((err) => {
        console.error("error fetching all clients: " + err);
      });
  }, [setAllClients]);

  return (
    <div>
      <TableContainer component={Paper}>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>Company Id</TableCell>
              <TableCell align="right">Client Company Name</TableCell>
              <TableCell align="right">Client City</TableCell>
              <TableCell align="right">Client Password</TableCell>
              <TableCell align="right">Money Owed</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {!!allClients.data &&
              (allClients.data || []).map((clients) => (
                <TableRow key={clients.clientCompId016}>
                  <TableCell component="th" scope="row">
                    {clients.clientCompId016}
                  </TableCell>
                  <TableCell align="right">
                    {clients.clientCompName016}
                  </TableCell>
                  <TableCell align="right">{clients.clientCity016}</TableCell>
                  <TableCell align="right">
                    {clients.clientCompPassword016}
                  </TableCell>
                  <TableCell align="right">${clients.moneyOwed016}</TableCell>
                </TableRow>
              ))}
          </TableBody>
        </Table>
      </TableContainer>
    </div>
  );
};

export default Clients;
