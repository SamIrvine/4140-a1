import React, { useState, useEffect } from "react";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import DatabaseApi from "../../api/DatabaseApi.js";
import FilledIcon from "@material-ui/icons/CheckCircleOutline";
import { Typography } from "@material-ui/core";

const PurchaseOrders016 = ({ isInsertOpen016 }) => {
  const [allOrders, setAllOrders] = useState([]);

  const fetchAllOrders016 = () => {
    DatabaseApi.getAllOrders016(
      window.sessionStorage.getItem("clientCompId016")
    )
      .then((allOrders) => {
        setAllOrders(allOrders.data);
      })
      .catch((err) => {
        console.error("error fetching all orders: " + err);
      });
  };

  useEffect(() => {
    if (!isInsertOpen016) {
      setTimeout(fetchAllOrders016, 500);
    }
    fetchAllOrders016();
  }, [setAllOrders, isInsertOpen016]);

  return (
    <TableContainer component={Paper}>
      <Table>
        <TableHead>
          <TableRow>
            <TableCell>Order Number</TableCell>
            <TableCell align="right">Order Date</TableCell>
            <TableCell align="right">Order Status</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {!!allOrders &&
            (allOrders || []).map((order) => {
              return (
                <TableRow key={order.poNo016}>
                  <TableCell component="th" scope="row">
                    {order.poNo016}
                  </TableCell>
                  <TableCell align="right">{order.datePo016}</TableCell>
                  <TableCell align="right">
                    {order.status === "filled" ? (
                      <div style={{ display: "flex", alignItems: "center" }}>
                        <Typography style={{ marginRight: "8px" }}>
                          {order.status}
                        </Typography>
                        <FilledIcon style={{ color: "green" }} />{" "}
                      </div>
                    ) : (
                      `${order.status}`
                    )}
                  </TableCell>
                </TableRow>
              );
            })}
        </TableBody>
      </Table>
    </TableContainer>
  );
};

export default PurchaseOrders016;
