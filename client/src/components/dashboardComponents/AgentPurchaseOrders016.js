import React, { useState, useEffect } from "react";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import DatabaseApi from "../../api/DatabaseApi.js";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import ProcessOrderDialog from "./updateDialogs/ProcessOrderDialog016";
import FilledIcon from "@material-ui/icons/CheckCircleOutline";

const AgentPurchaseOrders016 = () => {
  const [allOrders, setAllOrders] = useState([]);
  const [filteredOrders, setFilteredOrders] = useState([]);
  const [searchTerm, setSearchTerm] = useState("");
  const [processOrder, setProcessOrder] = useState({});
  const [isProcessOpen016, setIsProcessOpen016] = useState(false);

  const handleSearchChange = (event) => {
    setSearchTerm(event.target.value);
  };

  const onProcessPurchaseOrder = (order) => {
    setProcessOrder(order);
    setIsProcessOpen016(true);
  };

  const handleClose016 = () => {
    setProcessOrder({});
    setIsProcessOpen016(false);
  };

  const fetchAllOrders016 = () => {
    DatabaseApi.getAllOrdersAgent016()
      .then((allOrders) => {
        setAllOrders(allOrders.data);
      })
      .catch((err) => {
        console.error("error fetching all orders: " + err);
      });
  };

  useEffect(() => {
    if (!isProcessOpen016) {
      setTimeout(fetchAllOrders016, 500);
    }
    fetchAllOrders016();
  }, [setAllOrders, isProcessOpen016]);

  useEffect(() => {
    if (!!searchTerm) {
      setFilteredOrders(
        allOrders.filter((order) => order.poNo016 == searchTerm)
      );
    } else {
      setFilteredOrders([]);
    }
  }, [setFilteredOrders, searchTerm, allOrders]);

  return (
    <div style={{ width: "100%" }}>
      <TextField
        label="Search PO Number"
        variant="outlined"
        onChange={handleSearchChange}
        type="number"
        InputProps={{ inputProps: { min: 0 } }}
      ></TextField>
      <TableContainer component={Paper}>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>Order Number</TableCell>
              <TableCell align="right">Order Date</TableCell>
              <TableCell align="right">Order Status</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {!!allOrders &&
              (
                (filteredOrders.length > 0 ? filteredOrders : allOrders) || []
              ).map((order) => {
                return (
                  <TableRow key={order.poNo016}>
                    <TableCell component="th" scope="row">
                      {order.poNo016}
                    </TableCell>
                    <TableCell align="right">{order.datePo016}</TableCell>
                    <TableCell
                      style={{
                        color: order.status === "aborted" ? "red" : "initial",
                      }}
                      align="right"
                    >{`${order.status}`}</TableCell>
                    <TableCell>
                      {order.status === "filled" ? (
                        <div
                          style={{
                            display: "flex",
                            alignItems: "center",
                            justifyContent: "center",
                          }}
                        >
                          <FilledIcon
                            style={{
                              width: "28px",
                              height: "auto",
                              color: "green",
                            }}
                          />{" "}
                        </div>
                      ) : (
                        <Button
                          variant="contained"
                          color="secondary"
                          onClick={() => {
                            onProcessPurchaseOrder(order);
                          }}
                        >
                          Process
                        </Button>
                      )}
                    </TableCell>
                  </TableRow>
                );
              })}
          </TableBody>
        </Table>
      </TableContainer>
      <ProcessOrderDialog
        isOpen016={isProcessOpen016}
        onClose016={handleClose016}
        order016={processOrder}
      />
    </div>
  );
};

export default AgentPurchaseOrders016;
