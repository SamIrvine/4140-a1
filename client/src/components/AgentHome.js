import React, { useState } from "react";
import Button from "@material-ui/core/Button";
import Parts016 from "./dashboardComponents/Parts016";
import AgentPurchaseOrders016 from "./dashboardComponents/AgentPurchaseOrders016";
import Clients016 from "./dashboardComponents/Clients016";
import UpdatePartDialog016 from "./dashboardComponents/updateDialogs/UpdatePartDialog016";

const AgentHome = () => {
  const [showPurchaseOrders, setShowPurchaseOrders] = useState(false);
  const [showParts, setShowParts] = useState(false);
  const [showClients, setShowClients] = useState(false);
  const [isUpdateOpen016, setIsUpdateOpen016] = useState(false);

  const onShowPurchaseOrders016 = () => {
    setShowPurchaseOrders(!showPurchaseOrders);
    if (showClients || showParts) {
      setShowClients(false);
      setShowParts(false);
    }
  };

  const onShowParts016 = () => {
    setShowParts(!showParts);
    if (showClients || showPurchaseOrders) {
      setShowClients(false);
      setShowPurchaseOrders(false);
    }
  };

  const onShowClients016 = () => {
    setShowClients(!showClients);
    if (showParts || showPurchaseOrders) {
      setShowParts(false);
      setShowPurchaseOrders(false);
    }
  };

  const handleOpen016 = () => {
    setIsUpdateOpen016(true);
  };

  const handleClose016 = () => {
    setIsUpdateOpen016(false);
  };

  return (
    <div>
      <div style={{ display: "flex", justifyContent: "center" }}>
        <Button
          style={{ marginRight: "8px" }}
          variant="contained"
          color={showPurchaseOrders ? "secondary" : "default"}
          onClick={onShowPurchaseOrders016}
        >
          {showPurchaseOrders ? "Hide Purchase Orders" : "Show Purchase Orders"}
        </Button>

        <Button
          style={{ marginRight: "8px" }}
          variant="contained"
          color={showParts ? "secondary" : "default"}
          onClick={onShowParts016}
        >
          {showParts ? "Hide Parts" : "Show Parts"}
        </Button>

        <Button
          style={{ marginRight: "8px" }}
          variant="contained"
          color={showClients ? "secondary" : "default"}
          onClick={onShowClients016}
        >
          {showClients ? "Hide Clients" : "Show Clients"}
        </Button>

        <Button
          variant="contained"
          color={isUpdateOpen016 ? "secondary" : "default"}
          onClick={handleOpen016}
        >
          Update a part
        </Button>
      </div>
      <div style={{ display: "flex", padding: "16px 0px 16px 0px" }}>
        {showParts && <Parts016 isSpecificUpdateOpen016={isUpdateOpen016} />}
        {showClients && <Clients016 />}
        {showPurchaseOrders && <AgentPurchaseOrders016 />}
      </div>
      <UpdatePartDialog016
        isOpen016={isUpdateOpen016}
        onClose016={handleClose016}
      />
    </div>
  );
};

export default AgentHome;
