import React from "react";
import logo from "../images/logo192.png";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";

const Header = ({ isAuth, setAuth016, setIsAgent016 }) => {
  const handleLogout = () => {
    setAuth016(false);
    window.sessionStorage.removeItem("isAuth");
    setIsAgent016(false);
    window.sessionStorage.removeItem("isAgent");
    window.sessionStorage.removeItem("clientCompId016");
  };

  return (
    <div
      style={{
        display: "flex",
        padding: "16px",
        alignItems: "center",
        justifyContent: "center",
        boxShadow: "0 4px 2px -4px grey",
      }}
    >
      <Typography style={{ marginRight: "16px" }} variant="h2">
        Part-O-Matic
      </Typography>
      <img
        style={{ width: 64, height: "auto", marginRight: "32px" }}
        src={logo}
        alt="Wrench"
      ></img>
      {isAuth && (
        <Button
          onClick={handleLogout}
          style={{ color: "red", borderColor: "red", justifySelf: "flex-end" }}
          variant="outlined"
        >
          Logout
        </Button>
      )}
    </div>
  );
};

export default Header;
