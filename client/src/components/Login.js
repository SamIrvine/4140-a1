import React, { useState } from "react";
import TextField from "@material-ui/core/TextField";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import DatabaseApi from "../api/DatabaseApi";

const Login = ({ setAuth016, setIsAgent016 }) => {
  const [userName016, setUserName016] = useState("");
  const [password016, setPassword016] = useState("");
  const [errorMessage016, setErrorMessage016] = useState("");
  const [isAgentLogin016, setAgentLogin016] = useState(true);

  const onSubmit016 = () => {
    if (isAgentLogin016) {
      // make a request to the agent table and set auth on success
      DatabaseApi.handleAgentLogin(userName016, password016).then((result) => {
        if (result.data === "Success") {
          setAuth016(true);
          setIsAgent016(true);
          sessionStorage.setItem("isAuth", true);
          sessionStorage.setItem("isAgent", true);
        } else {
          setErrorMessage016("Invalid username or password.");
          setUserName016("");
          setPassword016("");
        }
      });
    } else {
      // make a request to the client table and set auth on success
      DatabaseApi.handleClientLogin(userName016, password016).then((result) => {
        if (!!result.data) {
          setAuth016(true);
          setIsAgent016(false);
          sessionStorage.setItem("clientCompId016", result.data);
          sessionStorage.setItem("isAuth", true);
          sessionStorage.setItem("isAgent", false);
        } else {
          setErrorMessage016("Invalid username or password.");
          setUserName016("");
          setPassword016("");
        }
      });
    }
  };

  return (
    <div
      style={{
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
      }}
    >
      <Typography variant="h4">
        {isAgentLogin016 ? "Agent " : "Customer "}Login
      </Typography>
      <br></br>
      <form onSubmit={onSubmit016} style={{ display: "flex" }}>
        <TextField
          value={userName016}
          onChange={(event) => setUserName016(event.target.value)}
          style={{ marginRight: "8px" }}
          id="username"
          label="Username"
          variant="outlined"
        />
        <TextField
          value={password016}
          onChange={(event) => setPassword016(event.target.value)}
          style={{ marginRight: "8px" }}
          type="password"
          id="password"
          label="Password"
          variant="outlined"
        />
        <Button onClick={onSubmit016} color="secondary" variant="contained">
          Submit
        </Button>
      </form>
      {!!errorMessage016 && (
        <>
          <br />
          <Typography color="error" variant="h5">
            {errorMessage016}
          </Typography>
        </>
      )}
      <Button
        onClick={() => setAgentLogin016(!isAgentLogin016)}
        color="primary"
        variant="contained"
        style={{ background: "#79A4AF", color: "", marginTop: "16px" }}
      >
        {isAgentLogin016 ? "Switch to Customer Login" : "Switch to Agent Login"}
      </Button>
    </div>
  );
};

export default Login;
