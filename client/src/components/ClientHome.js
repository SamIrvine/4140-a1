import React, { useState, useEffect } from "react";
import Button from "@material-ui/core/Button";
import PurchaseOrders016 from "./dashboardComponents/PurchaseOrders016";
import PurchaseOrderSearch016 from "./dashboardComponents/PurchaseOrderSearch016";
import InsertPurchaseDialog016 from "./dashboardComponents/updateDialogs/InsertPurchaseDialog016";
import DatabaseApi from "../api/DatabaseApi";
import Typography from "@material-ui/core/Typography";

const ClientHome = () => {
  const [showPurchaseOrders, setShowPurchaseOrders] = useState(false);
  const [showPurchaseOrderSearch, setShowPurchaseOrderSearch] = useState(false);
  const [isInsertOrderOpen, setIsInsertOrderOpen] = useState(false);

  const showPurchaseOrders016 = () => {
    setShowPurchaseOrders(!showPurchaseOrders);
    if (showPurchaseOrderSearch) {
      setShowPurchaseOrderSearch(false);
    }
  };

  const showPurchaseOrderSearch016 = () => {
    setShowPurchaseOrderSearch(!showPurchaseOrderSearch);
    if (showPurchaseOrders) {
      setShowPurchaseOrders(false);
    }
  };

  const handleOpen016 = () => {
    setIsInsertOrderOpen(true);
  };

  const handleClose016 = () => {
    setIsInsertOrderOpen(false);
  };

  const [clientInfo, setClientInfo] = useState({});

  const fetchClientInfo016 = () => {
    DatabaseApi.getOneClient016(
      window.sessionStorage.getItem("clientCompId016")
    )
      .then((client) => {
        setClientInfo(client.data);
      })
      .catch((err) => {
        console.error("error fetching client: " + err);
      });
  };

  useEffect(() => {
    if (!isInsertOrderOpen) {
      setTimeout(fetchClientInfo016, 500);
    }
    fetchClientInfo016();
  }, [setClientInfo, isInsertOrderOpen]);

  return (
    <div>
      <div style={{ marginBottom: "24px" }}>
        <Typography color="textPrimary" variant="h5">
          Your Info
        </Typography>
        <Typography color="textSecondary" variant="h6">
          {`Name: ${clientInfo.clientCompName016} | City: ${clientInfo.clientCity016} | `}{" "}
          <strong> {`Money Owed: $${clientInfo.moneyOwed016}`}</strong>
        </Typography>
      </div>
      <div style={{ display: "flex", justifyContent: "center" }}>
        <Button
          style={{ marginRight: "8px" }}
          variant="contained"
          color={showPurchaseOrders ? "secondary" : "default"}
          onClick={showPurchaseOrders016}
        >
          {showPurchaseOrders ? "Hide Purchase Orders" : "Show Purchase Orders"}
        </Button>

        <Button
          style={{ marginRight: "8px" }}
          variant="contained"
          color={showPurchaseOrderSearch ? "secondary" : "default"}
          onClick={showPurchaseOrderSearch016}
        >
          {showPurchaseOrderSearch
            ? "Hide Purchase Order Search"
            : "Show Purchase Order Search"}
        </Button>

        <Button
          variant="contained"
          color={isInsertOrderOpen ? "secondary" : "default"}
          onClick={handleOpen016}
        >
          Insert Purchase Order
        </Button>
      </div>
      <div style={{ display: "flex", padding: "16px 0px 16px 0px" }}>
        {showPurchaseOrders && (
          <PurchaseOrders016 isInsertOpen016={isInsertOrderOpen} />
        )}
        {showPurchaseOrderSearch && <PurchaseOrderSearch016 />}
      </div>
      <InsertPurchaseDialog016
        isOpen016={isInsertOrderOpen}
        onClose016={handleClose016}
      />
    </div>
  );
};

export default ClientHome;
