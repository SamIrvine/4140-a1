import axios from "axios";

class DatabaseApi {
  static getAllParts016() {
    return axios({
      method: "get",
      url: "http://localhost:4001/api/parts",
    }).then(function (response) {
      return response;
    });
  }

  static getAllOrders016(clientCompId016) {
    return axios({
      method: "get",
      url: `http://localhost:4001/api/purchaseOrders/${clientCompId016}`,
    }).then(function (response) {
      return response;
    });
  }

  static getAllOrdersAgent016() {
    return axios({
      method: "get",
      url: `http://localhost:4001/api/purchaseOrders`,
    }).then(function (response) {
      return response;
    });
  }

  static getAllLines016(poNo016) {
    return axios({
      method: "get",
      url: `http://localhost:4001/api/lines/${poNo016}`,
    }).then(function (response) {
      return response;
    });
  }

  static getAllClients016() {
    return axios({
      method: "get",
      url: "http://localhost:4001/api/clients",
    }).then(function (response) {
      return response;
    });
  }

  static getOnePart016(partNo016) {
    return axios({
      method: "get",
      url: `http://localhost:4001/api/parts/${partNo016}`,
    }).then(function (response) {
      return response;
    });
  }

  static getOneClient016(clientCompId016) {
    return axios({
      method: "get",
      url: `http://localhost:4001/api/clients/${clientCompId016}`,
    }).then(function (response) {
      return response;
    });
  }

  static updatePart016(partNo016, currentPrice016, qty016) {
    return axios({
      method: "put",
      url: `http://localhost:4001/api/parts/${partNo016}`,
      data: {
        currentPrice016,
        qty016,
      },
    }).then(function (response) {
      return response;
    });
  }

  static handleAgentLogin(agentUserName016, agentPassword016) {
    return axios({
      method: "get",
      url: `http://localhost:4001/api/agents/${agentUserName016}/${agentPassword016}`,
    }).then(function (response) {
      return response;
    });
  }

  static handleClientLogin(clientCompName016, clientCompPassword016) {
    return axios({
      method: "get",
      url: `http://localhost:4001/api/clients/${clientCompName016}/${clientCompPassword016}`,
    }).then(function (response) {
      return response;
    });
  }

  static handleCreatePurchaseOrder016(clientCompId016) {
    return axios({
      method: "post",
      url: `http://localhost:4001/api/purchaseOrders`,
      data: {
        clientCompId016,
      },
    }).then(function (response) {
      return response;
    });
  }

  static handleCreateLine016(poNo016, partNo016, linePrice016) {
    return axios({
      method: "post",
      url: `http://localhost:4001/api/lines`,
      data: {
        poNo016,
        partNo016,
        linePrice016,
      },
    }).then(function (response) {
      return response;
    });
  }

  static handleProcessPurchaseOrder016(poNo016, lines016) {
    return axios({
      method: "post",
      url: `http://localhost:4001/api/purchaseOrders/process/${poNo016}`,
      data: {
        lines016,
      },
    }).then(function (response) {
      return response;
    });
  }
}

export default DatabaseApi;
