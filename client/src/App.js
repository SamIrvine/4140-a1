import "./App.css";
import { useState } from "react";
import Header from "./components/Header";
import AgentHome from "./components/AgentHome";
import ClientHome from "./components/ClientHome";
import Login from "./components/Login";

const App = () => {
  const [isAuth, setAuth] = useState(window.sessionStorage.getItem("isAuth"));
  const [isAgent, setIsAgent] = useState(
    window.sessionStorage.getItem("isAgent")
  );

  const Home = () => {
    return isAgent === "true" || (typeof isAgent === "boolean" && isAgent) ? (
      <AgentHome />
    ) : (
      <ClientHome />
    );
  };

  return (
    <main>
      <Header setIsAgent016={setIsAgent} setAuth016={setAuth} isAuth={isAuth} />
      <div
        style={{ display: "flex", justifyContent: "center", padding: "24px" }}
      >
        {isAuth ? (
          <Home />
        ) : (
          <Login setAuth016={setAuth} setIsAgent016={setIsAgent} />
        )}
      </div>
    </main>
  );
};

export default App;
