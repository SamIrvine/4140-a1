# CSCI 4140 Client App - Sam Irvine B00757016

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

This project uses [Material UI](https://material-ui.com/) and [Axios](https://github.com/axios/axios) for HTTP requests.

Ensure you have a recent version of [node](https://nodejs.org/en/) installed on your local machine.

## Available Scripts

In the project directory, you can run:

### `npm install`

Installs the dependencies required to run the project.

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.
