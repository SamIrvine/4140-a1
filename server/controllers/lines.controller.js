const { lines } = require("../models/root");
const db = require("../models/root");
const Lines = db.lines;

//** line Fetching Methods */
exports.listAll = (req, res) => {
  const poNo016 = req.params.poNo016;
  lines
    .findAll({
      where: {
        poNo016: poNo016,
      },
    })
    .then((lines) => {
      res.send(lines);
    })
    .catch(() => {
      res.status(500).send({
        message: "Error fetching all lines with poNo016: " + poNo016,
      });
    });
};

exports.getOne = (req, res) => {
  const poNo016 = req.params.poNo016;
  const lineNo016 = req.params.lineNo016;

  Lines.findOne({
    where: { poNo016: poNo016, lineNo016: lineNo016 },
  })
    .then((line) => {
      res.send(line);
    })
    .catch(() => {
      res.status(500).send({
        message: "Error fetching line with lineNo016: " + line,
      });
    });
};

//** line Insert Methods */
exports.create = (req, res) => {
  if (!req.body.linePrice016 || !req.body.poNo016 || !req.body.partNo016) {
    res.status(400).send({
      message: "body missing linePrice016 || poNo016 || partNo016",
    });
    return;
  }

  const line = {
    linePrice016: req.body.linePrice016,
    poNo016: req.body.poNo016,
    partNo016: req.body.partNo016,
  };

  Lines.create(line)
    .then((line) => {
      res.send(line);
    })
    .catch((err) => {
      res.status(500).send({
        message: "An error occurred while creating the line " + err,
      });
    });
};

//** line Delete Methods */
exports.delete = (req, res) => {
  const poNo016 = req.params.poNo016;
  const lineNo016 = req.params.lineNo016;

  Lines.destroy({
    where: { poNo016: poNo016, lineNo016: lineNo016 },
  })
    .then((deleted) => {
      let message =
        deleted === 1
          ? "line deleted."
          : "line was not deleted, check lineNo016 || poNo016 to ensure it is correct";

      res.send({
        message,
      });
    })
    .catch(() => {
      res.status(500).send({
        message: "Error deleting line with lineNo016: " + lineNo016,
      });
    });
};
