const db = require("../models/root");
const Clients = db.clients;

//** Client Fetching Methods */
exports.listAll = (req, res) => {
  Clients.findAll()
    .then((Clients) => {
      res.send(Clients);
    })
    .catch(() => {
      res.status(500).send({
        message: "Error fetching all Clients",
      });
    });
};

exports.getOne = (req, res) => {
  const clientCompId016 = req.params.clientCompId016;
  Clients.findByPk(clientCompId016)
    .then((client) => {
      res.send(client);
    })
    .catch(() => {
      res.status(500).send({
        message:
          "Error fetching client with clientCompId016: " + clientCompId016,
      });
    });
};

exports.getOneLogin = (req, res) => {
  const clientCompName016 = req.params.clientCompName016;
  const clientCompPassword016 = req.params.clientCompPassword016;
  Clients.findAll({
    where: {
      clientCompName016: clientCompName016,
      clientCompPassword016: clientCompPassword016,
    },
  })
    .then((found) => {
      if (found.length == 1) {
        res.send(`${found[0].dataValues.clientCompId016}`);
      } else {
        res.send("Failure");
      }
    })
    .catch(() => {
      res.status(500).send({
        message:
          "Error fetching client invalid username or password: " +
          clientCompName016,
      });
    });
};

//** Client Insert Methods */
exports.create = (req, res) => {
  if (
    !req.body.clientCompName016 ||
    !req.body.moneyOwed016 ||
    !req.body.clientCity016 ||
    !req.body.clientCompPassword016
  ) {
    res.status(400).send({
      message:
        "body missing clientCompName016 || moneyOwed016 || clientCity016 || clientCompPassword016",
    });
    return;
  }

  const client = {
    clientCompName016: req.body.clientCompName016,
    moneyOwed016: req.body.moneyOwed016,
    clientCity016: req.body.clientCity016,
    clientCompPassword016: req.body.clientCompPassword016,
  };

  Clients.create(client)
    .then((client) => {
      res.send(client);
    })
    .catch((err) => {
      res.status(500).send({
        message: "An error occurred while creating the client " + err,
      });
    });
};

//** client Delete Methods */
exports.delete = (req, res) => {
  const clientCompId016 = req.params.clientCompId016;

  Clients.destroy({
    where: { clientCompId016: clientCompId016 },
  })
    .then((deleted) => {
      let message =
        deleted === 1
          ? "client deleted."
          : "client was not deleted, check clientCompId016 to ensure it is correct";

      res.send({
        message,
      });
    })
    .catch(() => {
      res.status(500).send({
        message:
          "Error deleting client with clientCompId016: " + clientCompId016,
      });
    });
};
