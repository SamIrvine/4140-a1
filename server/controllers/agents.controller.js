const db = require("../models/root");
const Agents = db.agents;

exports.getOne = (req, res) => {
  const agentUserName016 = req.params.agentUserName016;
  const agentPassword016 = req.params.agentPassword016;
  Agents.findAll({
    where: {
      agentUserName016: agentUserName016,
      agentPassword016: agentPassword016,
    },
  })
    .then((found) => {
      if (found.length == 1) {
        res.send("Success");
      } else {
        res.send("Failure");
      }
    })
    .catch(() => {
      res.status(500).send({
        message:
          "Error fetching agent invalid username or password: " +
          agentUserName016,
      });
    });
};

//** Agent Insert Methods */
exports.create = (req, res) => {
  if (!req.body.agentUserName016 || !req.body.agentPassword016) {
    res.status(400).send({
      message: "body missing agentUserName016 || agentPassword016",
    });
    return;
  }

  const agent = {
    agentUserName016: req.body.agentUserName016,
    agentPassword016: req.body.agentPassword016,
  };

  Agents.create(agent)
    .then((agent) => {
      res.send("Successfully created agent " + agent);
    })
    .catch((err) => {
      res.status(500).send({
        message: "An error occurred while creating the agent " + err,
      });
    });
};
