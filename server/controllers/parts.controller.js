const db = require("../models/root");
const Parts = db.parts;

//** Part Fetching Methods */
exports.listAll = (req, res) => {
  Parts.findAll()
    .then((parts) => {
      res.send(parts);
    })
    .catch(() => {
      res.status(500).send({
        message: "Error fetching all parts",
      });
    });
};

exports.getOne = (req, res) => {
  const partNo016 = req.params.partNo016;
  Parts.findByPk(partNo016)
    .then((part) => {
      res.send(part);
    })
    .catch(() => {
      res.status(500).send({
        message: "Error fetching part with partNo016: " + partNo016,
      });
    });
};

//** Part Insert Methods */
exports.create = (req, res) => {
  if (
    !req.body.partDescription016 ||
    !req.body.qty016 ||
    !req.body.partName016 ||
    !req.body.currentPrice016
  ) {
    res.status(400).send({
      message:
        "body missing partDesc || qty016 || partName016 || currentPrice016",
    });
    return;
  }

  const part = {
    partDescription016: req.body.partDescription016,
    qty016: req.body.qty016,
    partName016: req.body.partName016,
    currentPrice016: req.body.currentPrice016,
  };

  Parts.create(part)
    .then((part) => {
      res.send(part);
    })
    .catch((err) => {
      res.status(500).send({
        message: "An error occurred while creating the part " + err,
      });
    });
};

//** Part Delete Methods */
exports.delete = (req, res) => {
  const partNo016 = req.params.partNo016;

  Parts.destroy({
    where: { partNo016: partNo016 },
  })
    .then((deleted) => {
      let message =
        deleted === 1
          ? "Part deleted."
          : "Part was not deleted, check partNo016 to ensure it is correct";

      res.send({
        message,
      });
    })
    .catch(() => {
      res.status(500).send({
        message: "Error deleting part with partNo016: " + partNo016,
      });
    });
};

//** Part Update Methods */
exports.update = (req, res) => {
  const partNo016 = req.params.partNo016;

  Parts.update(
    { currentPrice016: req.body.currentPrice016, qty016: req.body.qty016 },
    { where: { partNo016 } }
  )
    .then((updated) => {
      let message =
        updated >= 1
          ? "Part updated."
          : "Part was not updated, check partNo016 to ensure it is correct, or the values have not changed";

      res.send({
        message,
      });
    })
    .catch(() => {
      res.status(500).send({
        message: "Error updating part with partNo016: " + partNo016,
      });
    });
};
