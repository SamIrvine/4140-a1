const { purchaseOrders } = require("../models/root");
const db = require("../models/root");
const PurchaseOrders = db.purchaseOrders;
const Parts = db.parts;

//** PurchaseOrder Fetching Methods */
exports.listAll = (req, res) => {
  const clientCompId016 = req.params.clientCompId016;
  purchaseOrders
    .findAll({
      where: {
        clientCompId016: clientCompId016,
      },
    })
    .then((purchaseOrders) => {
      res.send(purchaseOrders);
    })
    .catch(() => {
      res.status(500).send({
        message: "Error fetching all purchaseOrders",
      });
    });
};

exports.agentListAll = (req, res) => {
  purchaseOrders
    .findAll()
    .then((purchaseOrders) => {
      res.send(purchaseOrders);
    })
    .catch(() => {
      res.status(500).send({
        message: "Error fetching all purchaseOrders",
      });
    });
};

exports.getOne = (req, res) => {
  const purchaseOrderNo016 = req.params.purchaseOrderNo016;
  const clientCompId016 = req.params.clientCompId016;
  PurchaseOrders.findByPk(purchaseOrderNo016)
    .then((purchaseOrder) => {
      if (purchaseOrder.dataValues.clientCompId016 == clientCompId016)
        res.send(purchaseOrder);
      else
        res
          .status(401)
          .send({ message: "Not authorized to view POs for this company" });
    })
    .catch(() => {
      res.status(500).send({
        message:
          "Error fetching PurchaseOrder with PurchaseOrderNo016: " +
          purchaseOrderNo016,
      });
    });
};

//** PurchaseOrder Insert Methods */
exports.create = (req, res) => {
  if (!req.body.clientCompId016) {
    res.status(400).send({
      message: "body missing clientCompId016",
    });
    return;
  }

  const PurchaseOrder = {
    clientCompId016: req.body.clientCompId016,
  };

  PurchaseOrders.create(PurchaseOrder)
    .then((purchaseOrder) => {
      res.send(purchaseOrder);
    })
    .catch((err) => {
      res.status(500).send({
        message: "An error occurred while creating the PurchaseOrder " + err,
      });
    });
};

//** PurchaseOrder Delete Methods */
exports.delete = (req, res) => {
  const purchaseOrderNo016 = req.params.purchaseOrderNo016;

  PurchaseOrders.destroy({
    where: { poNo016: purchaseOrderNo016 },
  })
    .then((deleted) => {
      let message =
        deleted === 1
          ? "PurchaseOrder deleted."
          : "PurchaseOrder was not deleted, check PurchaseOrderNo016 to ensure it is correct";

      res.send({
        message,
      });
    })
    .catch(() => {
      res.status(500).send({
        message:
          "Error deleting PurchaseOrder with PurchaseOrderNo016: " +
          PurchaseOrderNo016,
      });
    });
};

exports.processPurchaseOrder = async (req, res) => {
  const lines016 = req.body.lines016;
  const poNo016 = req.params.poNo016;

  const transaction = await db.sequelize.transaction();
  try {
    // process the lines with the transaction as an option
    let shouldRollback = false;
    for (const line of lines016) {
      const { dataValues: lineToUpdate } = await Parts.findByPk(
        line.partNo016,
        {
          transaction,
        }
      );

      const newQuantity = lineToUpdate.qty016 - line.quantity016;
      if (newQuantity >= 0) {
        await Parts.update(
          {
            qty016: newQuantity,
          },
          { where: { partNo016: line.partNo016 }, transaction }
        );
      } else {
        shouldRollback = true;
      }
    }
    if (shouldRollback) throw new Error();
    else {
      PurchaseOrders.update({ status: "filled" }, { where: { poNo016 } })
        .then((updated) => {
          let message =
            updated >= 1
              ? "Purchase Order filled."
              : "Purchase Order was not found, check poNo016 to ensure it exists";
          if (updated >= 1) {
            transaction.commit();
            res.send({
              message,
            });
          } else {
            transaction.rollback();
          }
        })
        .catch(() => {
          transaction.rollback();
          res.status(500).send({
            message: "Error updating Purchase Order with poNo016: " + poNo016,
          });
        });
    }
  } catch (error) {
    // If the execution reaches this line, an error was thrown.
    // We rollback the transaction.
    PurchaseOrders.update({ status: "aborted" }, { where: { poNo016 } }).then(
      (updated) => {
        let message =
          updated >= 1
            ? "Purchase Order was rolled back due to insufficient quantity."
            : "Purchase Order was not found, check poNo016 to ensure it exists";
        transaction.rollback();
        res.send({
          message,
        });
      }
    );
  }
};
