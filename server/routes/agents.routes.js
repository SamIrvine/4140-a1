module.exports = (app) => {
  // We will need the agents controller to query our DB
  const agents = require("../controllers/agents.controller.js");
  // initialize the express router to create the REST API routes
  let router = require("express").Router();

  // get a agent with a specific agentNo and password
  router.get("/:agentUserName016/:agentPassword016", agents.getOne);

  // create a agent
  router.post("/", agents.create);

  app.use("/api/agents", router);
};
