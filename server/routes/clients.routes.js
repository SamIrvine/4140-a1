module.exports = (app) => {
  // We will need the clients controller to query our DB
  const clients = require("../controllers/clients.controller.js");
  // initialize the express router to create the REST API routes
  let router = require("express").Router();

  // list all the clients
  router.get("/", clients.listAll);

  // get a client with a specific clientNo
  router.get("/:clientCompId016", clients.getOne);

  // get a client with a specific clientName and clientPassword
  router.get("/:clientCompName016/:clientCompPassword016", clients.getOneLogin);

  // create a client
  router.post("/", clients.create);

  // delete a client
  router.delete("/:clientCompId016", clients.delete);

  app.use("/api/clients", router);
};
