module.exports = (app) => {
  // We will need the parts controller to query our DB
  const parts = require("../controllers/parts.controller.js");
  // initialize the express router to create the REST API routes
  let router = require("express").Router();

  // list all the parts
  router.get("/", parts.listAll);

  // get a part with a specific partNo016
  router.get("/:partNo016", parts.getOne);

  // create a part
  router.post("/", parts.create);

  // delete a part
  router.delete("/:partNo016", parts.delete);

  // update a part
  router.put("/:partNo016", parts.update);

  app.use("/api/parts", router);
};
