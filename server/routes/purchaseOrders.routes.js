module.exports = (app) => {
  // We will need the purchaseOrders controller to query our DB
  const purchaseOrders = require("../controllers/purchaseOrders.controller.js");
  // initialize the express router to create the REST API routes
  let router = require("express").Router();

  // list all the purchaseOrders for clientCompany
  router.get("/:clientCompId016", purchaseOrders.listAll);

  // list all the purchaseOrders for all companies
  router.get("/", purchaseOrders.agentListAll);

  // get a purchaseOrder with a specific purchaseOrderNo016
  router.get("/:purchaseOrderNo016/:clientCompId016", purchaseOrders.getOne);

  // create a purchaseOrder
  router.post("/", purchaseOrders.create);

  // delete a purchaseOrder
  router.delete("/:purchaseOrderNo016", purchaseOrders.delete);

  // process a purchaseOrder
  router.post("/process/:poNo016", purchaseOrders.processPurchaseOrder);

  app.use("/api/purchaseOrders", router);
};
