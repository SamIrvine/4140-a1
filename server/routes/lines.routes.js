module.exports = (app) => {
  // We will need the lines controller to query our DB
  const lines = require("../controllers/lines.controller.js");
  // initialize the express router to create the REST API routes
  let router = require("express").Router();

  // list all the lines
  router.get("/:poNo016", lines.listAll);

  // get a line with a specific lineNo016 and poNo016
  router.get("/:lineNo016/:poNo016", lines.getOne);

  // create a line
  router.post("/", lines.create);

  // delete a line
  router.delete("/:lineNo016/:poNo016", lines.delete);

  app.use("/api/lines", router);
};
