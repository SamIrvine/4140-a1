const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const db = require("./models/root.js");
const localIpUrl = require("local-ip-url");
const app = express();

// The CORS options designate which domains the server will accept requests from
let corsSettings = {
  origin: "http://localhost:3000",
};

// Enable the use of our CORS policy
app.use(cors(corsSettings));

// Allows the server to parse JSON content
app.use(bodyParser.json());
try {
  db.sequelize.authenticate();
  console.log("Connection has been established successfully.");
} catch (error) {
  console.error("Unable to connect to the database:", error);
}

// Sync the DB with the existing models
db.sequelize.sync();

// require the api routes
require("./routes/parts.routes")(app);
require("./routes/clients.routes")(app);
require("./routes/agents.routes")(app);
require("./routes/purchaseOrders.routes")(app);
require("./routes/lines.routes")(app);

// Set the port the server listens to
const PORT = process.env.PORT || 15067;

let server = app.listen(PORT, () => {
  var host = server.address().address;
  var port = server.address().port;
  console.log(localIpUrl());
  console.log("running at http://" + host + ":" + port);
});
