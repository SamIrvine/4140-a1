module.exports = (sequelize, Sequelize) => {
  const Clients = sequelize.define(
    "clients016",
    {
      clientCompId016: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      clientCompName016: {
        type: Sequelize.STRING,
      },
      clientCity016: {
        type: Sequelize.STRING,
      },
      clientCompPassword016: {
        type: Sequelize.STRING,
      },
      moneyOwed016: {
        type: Sequelize.INTEGER,
      },
    },
    {
      freezeTableName: true,
    }
  );

  return Clients;
};
