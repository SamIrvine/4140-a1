module.exports = (sequelize, Sequelize) => {
  const Parts = sequelize.define(
    "parts016",
    {
      partNo016: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      partDescription016: {
        type: Sequelize.STRING,
      },
      partName016: {
        type: Sequelize.STRING,
      },
      currentPrice016: {
        type: Sequelize.INTEGER,
      },
      qty016: {
        type: Sequelize.INTEGER,
      },
    },
    {
      freezeTableName: true,
    }
  );

  return Parts;
};
