module.exports = (sequelize, Sequelize) => {
  const Agents = sequelize.define(
    "agents016",
    {
      agentUserName016: {
        primaryKey: true,
        type: Sequelize.STRING,
      },
      agentPassword016: {
        type: Sequelize.STRING,
      },
    },
    {
      freezeTableName: true,
    }
  );

  return Agents;
};
