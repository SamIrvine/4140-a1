module.exports = (sequelize, Sequelize) => {
  const Lines = sequelize.define(
    "lines016",
    {
      lineNo016: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      linePrice016: {
        type: Sequelize.INTEGER,
      },
      poNo016: {
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
    },
    {
      freezeTableName: true,
    }
  );

  const purchaseOrders = require("./purchaseOrders.model")(
    sequelize,
    Sequelize
  );
  const parts = require("./parts.model")(sequelize, Sequelize);

  Lines.belongsTo(purchaseOrders, { foreignKey: "poNo016" }); // Adds fk to purchase order
  Lines.belongsTo(parts, { foreignKey: "partNo016" });

  return Lines;
};
