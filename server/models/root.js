const config = require("../config.js");
const Sequelize = require("sequelize");

// Create our sequelize connection
const sequelize = new Sequelize(config.DB, config.USER, config.PASSWORD, {
  host: config.HOST,
  dialect: config.dialect,
  pool: {
    max: config.pool.max,
    min: config.pool.min,
    acquire: config.pool.acquire,
    idle: config.pool.idle,
  },
});

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.parts = require("./parts.model.js")(sequelize, Sequelize);
db.clients = require("./clients.model.js")(sequelize, Sequelize);
db.agents = require("./agents.model.js")(sequelize, Sequelize);
db.purchaseOrders = require("./purchaseOrders.model.js")(sequelize, Sequelize);
db.lines = require("./lines.model.js")(sequelize, Sequelize);

module.exports = db;
