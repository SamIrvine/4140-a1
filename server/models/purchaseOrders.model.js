const { DataTypes } = require("sequelize");

module.exports = (sequelize, Sequelize) => {
  const PurchaseOrders = sequelize.define(
    "purchaseOrders016",
    {
      poNo016: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      datePo016: {
        type: Sequelize.DATE,
        defaultValue: DataTypes.NOW,
      },
      status: {
        type: Sequelize.STRING,
        defaultValue: "placed",
      },
    },
    {
      freezeTableName: true,
    }
  );

  const clients = require("./clients.model")(sequelize, Sequelize);

  PurchaseOrders.belongsTo(clients, { foreignKey: "clientCompId016" }); // Adds fk to purchase order

  return PurchaseOrders;
};
