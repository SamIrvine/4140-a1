# CSCI 4140 Server App - Sam Irvine B00757016

This project uses [Express](https://expressjs.com/) and [Sequelize](https://sequelize.org/) as an ORM for DAL MySQL database.

Ensure you have a recent version of [node](https://nodejs.org/en/) installed on your local machine.

## Available Scripts

In the project directory, you can run:

### `npm install`

Installs the dependencies required to run the project.

### `npm start`

Runs the server, the tables will be created if they don't exist.
The server runs on [http://localhost:4001](http://localhost:4001) hitting this endpoint will not display anything in the browser.

To test the endpoints run the server, ensure you are connected to VPN, and hit the endpoints from the client app (see client README) or Postman collection that is provided.
